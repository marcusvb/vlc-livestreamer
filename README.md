vlc-livestreamer
================

A quick combination of programs, which will allow you to downloaded a selected livestream (preferably) from Twitch.tv. This can be run on a Linux server for ease!

================

(INSTALLATION):

Steps to make this work:

1. Make a new folder somewhere where you want to run the script from and have the files initially saved in.

	mkdir /home/*username*/Documents/twitch

2. place the twitch.sh Shell script in the folder

3. Make sure to have VLC player installed, as it uses this to capture and save the flash streams

	sudo apt-get update
	sudo apt-get install vlc browser-plugin-vlc

4. In order to avoid name confilicts with downloading of files, I use a password generator as set it as the name. Make sure to install this program to have it be able to use it in the bash scrpt.

	sudo apt-get install makepasswd

5. Download and install Livesteamer, a program which will do the translation of the Twitch streams and exectuion of VLC.

	sudo apt-get install python-pip
	sudo pip install livestreamer

6. Make small changes to the twitch.sh file in order to complete it to your needs. If you only want the files in the made directory, place a "#" infront of "transfer" on line 19.

7. Run the command to make it runable

	chmod+x twitch.sh

8. Run the program:

	./twitch.sh

