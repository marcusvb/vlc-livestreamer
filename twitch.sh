#!/bin/bash

transfer(){
	echo "Moving File(s)"
	sudo mv -i /twitch/*.flv /var/www/owncloud/data/*** 
	#Here you can move it 
	#to for instance, a data folder of an owncloud user, to have it autosync to your PC. 
	echo "Filemovement completed. Continuing to check Twitch.tv"
}

twitch(){
	#replace "riotgames" for the username of the streamer
	livestreamer twitch.tv/riotgames \
	source -o $(makepasswd -char=10).flv --yes-run-as-root	

	sleep 30
	#amount of time the program lies still in seconds

	transfer
	#call the transfer function to move the files to your wanted folder. You can remove this
	#if you don't want the files transfered anywhere.

	sleep 30
	
	twitch
	#recursion of the function calling itself to keep checking the Twitch User.
}

twitch
#calls the function for the first time
